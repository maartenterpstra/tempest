# TEMPEST

TEMPEST is a multi-resolution convolutional neural network used to estimate 3D deformation vector fields (DVFs) from highly undersampled MRI. 
The DVFs represent the underlying (respiratory) motion. Estimating and resolving this motion finds applications in, for example, enabling real-time adaptive MRI-guided radiotherapy.

This approach is proposed and evaluated in the publication by Terpstra et al. "Real-time 3D motion estimation from undersampled MRI using multi-resolution neural networks" (https://doi.org/10.1002/mp.15217)

# How to run
- Download the Jupyter Notebook
- Run the Notebook

Running this code assumes the presence of a GPU and training data.
The structure that is assumed of the training data is described in the Notebook.
